# UI Dockerfile

FROM python:3.6.0-alpine

COPY . /app

WORKDIR /app

RUN pip install -r ./requirements.txt

ENV MONGO_DATABASE_HOST 127.0.0.1
ENV MONGO_DATABASE_PORT 27017
ENV FLASK_APP=ui.py

WORKDIR /app/ui
#CMD ["python3", "ui/post_app.py"]
CMD ["gunicorn", "ui:app", "-b", "0.0.0.0"]
